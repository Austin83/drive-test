﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrivesTest
{
    class DiskSpace
    {
        private List<int> used;
        private List<int> total;

        public void ShowResults()
        {
            Console.WriteLine("[{0}]", string.Join(", ", used));
            Console.WriteLine("[{0}]", string.Join(", ", total));
            Console.WriteLine("Result: " + this.MinDrives());
            Console.WriteLine();
        }

        //Constructor
        public DiskSpace(int[] used, int[] total)
        {
            //Data validation
            if( used.Count() < Constants.DiskElementsMinimum)
            {
                throw new Exception("Parameter 'used' contains less than "+Constants.DiskElementsMinimum+ "  elements");
            }

            if (used.Count() > Constants.DiskElementsMaximum)
            {
                throw new Exception("Parameter 'used' contains more than " + Constants.DiskElementsMaximum+ " elements");
            }

            if( used.Count() != total.Count())
            {
                throw new Exception("Parameters 'used' and 'total' do not contain the same amount of elements");
            }

            for( int x = 0; x < used.Count(); x++)
            {
                if( used[x] < Constants.DiskSpaceMinimum)
                {
                    throw new Exception("Used space in disk in position " + x + " is smaller than "+Constants.DiskSpaceMinimum);
                }

                if (used[x] > Constants.DiskSpaceMaximum)
                {
                    throw new Exception("Used space in disk in position " + x + " is bigger than "+Constants.DiskSpaceMaximum);
                }

                if (total[x] < Constants.DiskSpaceMinimum)
                {
                    throw new Exception("Total space in disk in position " + x + " is smaller than "+Constants.DiskSpaceMinimum);
                }

                if (total[x] > Constants.DiskSpaceMaximum)
                {
                    throw new Exception("Total space in disk in position " + x + " is bigger than "+Constants.DiskSpaceMaximum);
                }

                if( used[x] > total[x])
                {
                    throw new Exception("Used sapce in disk in position " + x + "is bigger than it's total space");
                }
            }

            this.used = used.ToList<int>();
            this.total = total.ToList<int>();
        }

        public int MinDrives()
        {
            //Which is the disk with most free space?
            int diskWithMoreFreeSpace = this.MostFreeSpaceDisk();
            //Can this disk contain all the information?
            if( this.CanContainAll(diskWithMoreFreeSpace))
            {
                return 1;
            }

            //Is there only one not empty nor full disk?
            //Can the smallest used space of a disk be contained in the total free space of all the other disks?
            while(this.NotFullNorEmptyDisks() > 1 && this.SmallestIsContainable())
            {
                //Get the most free disk
                //diskWithMoreFreeSpace = this.MostFreeSpaceDisk();
                int fullestDisk = this.DiskFullest();

                //Is there a best fit disk for the selected one?
                int bestFit = this.BestFit(fullestDisk);
                
                //If there is, Transfer the data of a disk to the other and continue to the next iteration. 
                if( bestFit != Constants.Invalid)
                {
                    TransferData(bestFit, fullestDisk);
                }
                else //If there is not, Transfer the data of de disk with least used space to the most free disk.
                {
                    int smallestUsed = DiskWithSmallestUsedSpace(fullestDisk);
                    if( smallestUsed == Constants.Invalid)
                    {
                        break;
                    }
                    TransferData(smallestUsed, fullestDisk);
                }
            }

            //How many disks are not empty?
            int notEmpty = NotEmptyDisks();
            return notEmpty;
        }

        //Which is the disk with most free space?
        private int MostFreeSpaceDisk()
        {
            int diskWithMoreFreeSpace = 0;
            for(int index=1; index < this.used.Count(); index++)
            {
                if( (total[diskWithMoreFreeSpace] - used[diskWithMoreFreeSpace]) < (total[index] - used[index]))
                {
                    diskWithMoreFreeSpace = index;
                }
            }
            return diskWithMoreFreeSpace;
        }

        //Can this disk contain all the information?
        private bool CanContainAll(int selected)
        {
            int sum = this.used.Sum();
            sum -= used[selected];
            return sum <= (total[selected] - used[selected]);
        }

        //Which and how many disks are not empty nor full?
        private int NotFullNorEmptyDisks()
        {
            List<int> notFullNorEmptyDisks = new List<int>();
            for(int index=0; index < used.Count(); index++)
            {
                if( used[index] != total[index] && used[index] != 0)
                {
                    notFullNorEmptyDisks.Add(index);
                }
            }
            return notFullNorEmptyDisks.Count;
        }

        //Can the smallest used space of a disk be contained in the total free space of all the other disks?
        private bool SmallestIsContainable()
        {
            int diskWithSmallestUsedSpace = this.DiskWithSmallestUsedSpace();
            if( diskWithSmallestUsedSpace == Constants.Invalid)
            {
                return false;
            }
            bool result = used[diskWithSmallestUsedSpace] <= this.SumOfFreeSpaceExceptOneDisk(diskWithSmallestUsedSpace);
            return result;
        }

        //Which is the disk with the smallest used space?
        private int DiskWithSmallestUsedSpace()
        {
            int diskWithSmallestUsedSpace = 0;
            while( used[diskWithSmallestUsedSpace] == 0 && (diskWithSmallestUsedSpace+1) < used.Count)
            {
                diskWithSmallestUsedSpace++;
            }

            if (diskWithSmallestUsedSpace == used.Count || used[diskWithSmallestUsedSpace] == 0)
            {
                return Constants.Invalid;
            }

            for (int index=1; index < used.Count; index++)
            {
                if( used[diskWithSmallestUsedSpace] > used[index] && used[index] != 0)
                {
                    diskWithSmallestUsedSpace = index;
                }
            }

            return diskWithSmallestUsedSpace;
        }

        //Which is the disk fullest?
        private int DiskFullest()
        {
            int fullest = 0;

            for (int index = 0; index < used.Count; index++)
            {
                if (used[index] < total[index])
                {
                    fullest = index;
                    break;
                }
            }

            for (int index = 0; index < used.Count; index++)
            {
                if (used[fullest] < used[index] && used[index] < total[index])
                {
                    fullest = index;
                }
            }

            return fullest;
        }

        //Which is the disk with the smallest used space? Apply exception
        private int DiskWithSmallestUsedSpace(int exception)
        {
            int diskWithSmallestUsedSpace = 0;
            while ((used[diskWithSmallestUsedSpace] == 0 || diskWithSmallestUsedSpace == exception) && (diskWithSmallestUsedSpace + 1) < used.Count)
            {
                diskWithSmallestUsedSpace++;
            }

            if( diskWithSmallestUsedSpace == used.Count)
            {
                return Constants.Invalid;
            }

            for (int index = 1; index < used.Count; index++)
            {
                if ( index != exception && used[diskWithSmallestUsedSpace] > used[index] && used[index] != 0)
                {
                    diskWithSmallestUsedSpace = index;
                }
            }

            return diskWithSmallestUsedSpace;
        }

        //What is the sum of free space of every disk except selected?
        private int SumOfFreeSpaceExceptOneDisk(int selected)
        {
            int sum = 0;
            for(int index=0; index < used.Count(); index++)
            {
                sum += total[index] - used[index]; 
            }
            sum -= total[selected] - used[selected];
            return sum;
        }

        //How many disks are not empty?
        private int NotEmptyDisks()
        {
            int result = used.Where(i => i != 0).Count();
            return result;
        }

        //Is there a best fit disk for the selected one?
        private int BestFit(int selected)
        {
            int selectedFreeSpace = total[selected] - used[selected];
            int bestFit = Constants.Invalid;
            for(int index = 0; index < used.Count; index++)
            {
                if( index != selected)
                {
                    if (bestFit == Constants.Invalid && used[index] != 0 && used[index] <= selectedFreeSpace )
                    {
                        bestFit = index;
                    }
                    else
                    {
                        if (bestFit != Constants.Invalid && used[index] != 0 && used[index] <= selectedFreeSpace && used[index] > used[bestFit])
                        {
                            bestFit = index;
                        }
                    }
                }
            }
            return bestFit;
        }

        //Transfer de data of a disk to the other
        private void TransferData(int sender, int receptor)
        {
            int receptorFreeSpace = total[receptor] - used[receptor];
            if( used[sender] <= receptorFreeSpace)
            {
                used[receptor] += used[sender];
                used[sender] = 0;
            }
            else
            {
                used[receptor] = total[receptor];
                used[sender] -= receptorFreeSpace;
            }
        }
    }
}
