﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrivesTest
{
    public class Constants
    {
        public static int DiskElementsMinimum = 1;
        public static int DiskElementsMaximum = 50;
        public static int DiskSpaceMinimum = 1;
        public static int DiskSpaceMaximum = 1000;
        public static int Invalid = -1; 
    }
}
