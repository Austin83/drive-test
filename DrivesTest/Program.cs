﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrivesTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DiskSpace test0 = new DiskSpace(new int[] { 300, 525, 110 }, new int[] { 350, 600, 115 });
                Console.WriteLine("Test 0");
                test0.ShowResults();
                DiskSpace test1 = new DiskSpace(new int[] { 1, 200, 200, 199, 200, 200 }, new int[] { 1000, 200, 200, 200, 200, 200 });
                Console.WriteLine("Test 1");
                test1.ShowResults();
                DiskSpace test2 = new DiskSpace(new int[] { 750, 800, 850, 900, 950 }, new int[] { 800, 850, 900, 950, 1000 });
                Console.WriteLine("Test 2");
                test2.ShowResults();
                DiskSpace test3 = new DiskSpace(
                    new int[] { 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49 }, 
                    new int[] { 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50 });
                Console.WriteLine("Test 3");
                test3.ShowResults();
                DiskSpace test4 = new DiskSpace(
                    new int[] { 331, 242, 384, 366, 428, 114, 145, 89, 381, 170, 329, 190, 482, 246, 2, 38, 220, 290, 402, 385 },
                    new int[] { 992, 509, 997, 946, 976, 873, 771, 565, 693, 714, 755, 878, 897, 789, 969, 727, 765, 521, 961, 906 });
                Console.WriteLine("Test 4");
                test4.ShowResults();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            Console.WriteLine("Press Enter to finish");
            Console.ReadLine();
        }
    }
}
